package com.evobanco.zephyr.zephyrdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZephyrdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZephyrdemoApplication.class, args);
    }

}
